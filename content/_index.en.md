+++
author = "RobinForGood"
description = "Collection of tools and publications on GNU/Linux, Technological Sovereignty, Software Development, System Administration, DevOps, Privacy, Security, Academic research, Data analysis, Philosophy and a Blog." # set your site's description here. will be use for home page content meta tags (seo).
+++

# Knowledge Wiki

Collection of tools and publications on GNU/Linux, Technological Sovereignty, Software Development, System Administration, DevOps, Privacy, Security, Academic research, Data analysis, Philosophy and a Blog.

![Logo](images/logo.png?height=30)

## Table of contents

{{% children depth="4" %}}