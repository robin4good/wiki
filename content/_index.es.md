+++
author = "RobinForGood"
description = "Recopilación de herramientas y publicaciones sobre GNU/Linux, Soberanía Tecnológica, Desarrollo de software, Administración de sistemas, DevOps, Privacidad, Seguridad, Investigación académica, Análisis de datos, Filosofía y un Blog." # set your site's description here. will be use for home page content meta tags (seo).
+++

# Wiki de conocimiento

Recopilación de herramientas y publicaciones sobre GNU/Linux, Soberanía Tecnológica, Desarrollo de software, Administración de sistemas, DevOps, Privacidad, Seguridad, Investigación académica, Análisis de datos, Filosofía y un Blog.

![Logo](images/logo.png?height=30)

## Tabla de contenidos

{{% children depth="4" %}}