+++
title = "Soberanía Tecnológica"
date = 2022-05-28T16:07:44+02:00
weight = 10
chapter = true
pre = "<b> </b>"
+++

# Soberanía Tecnológica

## Recopilación de herramientas

* [Colección de herramientas para autoalojar](https://github.com/awesome-selfhosted/awesome-selfhosted)
* [CapRover](https://caprover.com/docs/get-started.html)
* [Cloudron.io](https://www.cloudron.io/)
* [Sandstorm.io](https://sandstorm.io/)

## Tabla de contenidos

{{% children depth="4" %}}
