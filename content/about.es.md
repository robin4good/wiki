+++
title = "Acerca de ..."
description = "Recopilación de herramientas y publicaciones sobre Programación, DevOps, Soberanía Tecnológica, Privacidad y Ciberseguridad"
date = "2020-09-26"
aliases = ["about-us", "about-hugo", "contact"]
author = "RobinForGood"
+++

En esta página web encontrarás una recopilación de herramientas y publicaciones sobre Programación, DevOps, Soberanía Tecnológica, Privacidad y Ciberseguridad. 

Si te gustaría colaborar y editar o añadir más contenido, escríbeme al correo robinforgood@protonmail.com con tu propuesta. ¡Gracias!