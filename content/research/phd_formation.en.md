---
title: "Phd_formation"
date: 2021-04-07T09:40:13+02:00
draft: true
---

Libros recomendados:
- Ramol y Cajal - Los tónicos de la voluntad
- Descartes - El método 
- Ortega y Gasset -  La universidad

## Objetivos de investigación

Siglas SMART (específico, medibles, alcanzable, realístico, actual)
Espresarlos en infinitivo.

### Alcance o metas

- Generales 
- Intermedias

### Cualidades y precauciones

- Medibles
- Accesibles o realizables
- Coherencia
- Temática que sea de interes para el investigador

## Clasificación metodológica

- Variable dependiente
- Variable independiente. Aquella que se escoge para estudiar la dependiente. El objetivo que queremos perseguir.
- Variable extraña. Aquellas que en el caso concreto de estudio no son relevantes pero la literatura la incluye como una varible que afecta a la variable dependiente. Según su nivel control:
  - Variable de control. Aquella que la literatura sabe que influye. Se sabe su efecto.
  - Variable de moderadora. Cuando la literatura apunta que puede haber un impacto pero no se sabe cuanto afecta. 
  - Variable de intervinientes.

## Hipótesis

- Cómo impactan las variables independientes en las variables dependientes.
- Habitualmente se suele utilizar una hipótesis por objetivo.
- Incluir las VD y VI en la hipótesis.
- Tener coherencia con el problema planteado. En caso de no tener tanta coherencia, reformular el problema e incluir relaciones del problema en la propia hipótesis.

## Introducción y problemas de investigación

- Enganchar al lector desde el principio. Plantear el trabajo de manera atractiva.
