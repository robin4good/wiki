---
title: "Bibliography"
date: 2021-02-17T09:12:29+01:00
draft: false
weight: 10
---

### Zotero

Zotero “is a free and open-source reference management software to manage bibliographic data and related research materials”<cite> [^1]</cite>. It is recommended to use File Syncing with WebDAV to have more free space. 

There are many useful [plugins for Zotero](https://www.zotero.org/support/plugins). Some recommended plugins are:

#### Latex
- [BetterBitTex](https://retorque.re/zotero-better-bibtex/). It is highly recommended add auto-export of the bibliography and use git.
  
#### Manage files
- [Zotfile](http://zotfile.com/) "plugin to manage your attachments: automatically rename, move, and attach PDFs (or other files) to Zotero items, sync PDFs from your Zotero library to your (mobile) PDF reader (e.g. an iPad, Android tablet, etc.) and extract annotations from PDF files"<cite> [^2]</cite>.

#### Citations Counters 
- [Zotero Citation Counts Manager](https://github.com/eschnett/zotero-citationcounts)
- Zotero Scholar Citation - DEPRECATED


[^1]: [Wikipedia, Zotero](https://en.wikipedia.org/wiki/Zotero)
[^2]: [GitHub, Zotfile](https://github.com/jlegewie/zotfile)

