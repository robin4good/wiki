---
title: "Search engines"
date: 2021-02-12T14:06:49+01:00
draft: false
weight: 5
---

### Where look for peer-reviewed academic literature?

- [Scopus](https://www.scopus.com/sources)
- [Web of Science](https://login.webofknowledge.com/)
- [Google Scholar](https://scholar.google.com/)
- [Institute of Electrical and Electronics Engineers (IEEE) Xplore](https://ieeexplore.ieee.org/Xplore/home.jsp)
- [Association for Computing Machinery (ACM) Digital Library](https://dl.acm.org/)
- [ScienceDirect](https://www.sciencedirect.com/)
- [dblp Computer Science Bibliography](https://dblp.org/)
- [Springer Link](https://link.springer.com/)
- [Wiley Online Library](https://onlinelibrary.wiley.com/)
- [The collection of Computer Science Bibliography](https://liinwww.ira.uka.de/bibliography/)
- [Open Access Button](https://openaccessbutton.org/)


#### Open-access pre-print repositories

- [ArXiv.org](https://arxiv.org/)
- [Archive ouverte HAL](https://hal.archives-ouvertes.fr/)

### Where to look for books?

- [Open Libra](https://openlibra.com/en/)

### How visualize the connections between academic papers?

- [Connected papers](https://www.connectedpapers.com/)


### What industry electronic databases exist?

- [Gartner](industry–www.gartner.com/)
- [OVUM](industry–www.ovum.com/)