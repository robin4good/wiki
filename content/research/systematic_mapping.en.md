---
title: "Systematic Mappings"
date: 2021-10-18T09:12:29+01:00
draft: true
weight: 15
---

## Systematic Mapping

Use Zotero with BetterBibTex plugin to add extra items. 

```typescript
if (Translator.BetterTeX) {
  reference.add({ name: 'keywords', value: item.tags, sep: ', ', enc: 'tags' });
}
```