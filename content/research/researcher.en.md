---
title: "Researcher Profile"
date: 2021-02-17T11:25:44+01:00
draft: true
---

### Create a researcher profile

It's recommended to register in the following platforms:

- ORCID

- ResearcherID

- Scopus

- Web of Science

- [Publons(https://publons.com/)]

- Google Scholar Citations

### How to measure the impact of authors and their papers?

- Index H 
- Science Citation Index (SCI)
- Impact of scientific journals

