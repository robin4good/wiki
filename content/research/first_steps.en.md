---
title: "First Steps"
date: 2021-02-17T09:13:29+01:00
draft: true
weight: 15
---


### How to start to research about a topic?

1. Go to a paper [search engine or database](../search_engines.en.md) and do a specific search with keywords and "" or the advanced search.
2. Look for surveys or literature reviews of the research topic.
3. Do a quick see to the abstract, body and conclusion and write a short note about the paper.
4. Select the papers that are more close to your research topic.