---
title: "Papers_structure"
date: 2021-02-17T11:25:44+01:00
draft: true
---

[Guide for Authors](https://www.elsevier.com/journals/computers-and-operations-research/0305-0548/guide-for-authors)

## Surveys, Review or Literature Review

The terms ‘Review’, ‘Literature Review’, and ‘Survey’ are interchangeable in this context.

[What makes a good survey?](https://www.journals.elsevier.com/computers-and-operations-research/what-makes-a-good-survey/)

- A survey may typically contain the following elements:
- Be approximately 50 printed pages in length (i.e. 100 typed pages)
- A good descriptive title
- A concise abstract
- A table of contents that establishes the structure of the survey
- Introduction (including motivation and historical remarks)
- Outline of the Survey
- Basic concepts, examples and results (with sketches of the proofs)
- Comments on the relevance of the results, relations to other results and applications
- Open problems
- Critical review of the relevant literature
Comprehensive bibliography 
