---
title: "LaTeX Configuration"
date: 2021-02-17T13:58:35+01:00
draft: true
---

## Configure VSCode as a LaTeX IDE

Install the following packages on Debian based:
+ texlive-full
+ latexmk

```bash
sudo apt install -y texlive-full latexmk 
```

Add some extensions:
+ [Latex Workshop](https://marketplace.visualstudio.com/items?itemName=James-Yu.latex-workshop)
+ [LTex](https://marketplace.visualstudio.com/items?itemName=valentjn.vscode-ltex)
+ [Todo Tree](https://marketplace.visualstudio.com/items?itemName=Gruntfuggly.todo-tree)

Select word-wrap to see the text in unique windows:
`-> Settings -> Search= wrap -> Editor: Word Wrap -> on `
