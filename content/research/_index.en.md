+++
title = "Academic Research"
date = 2021-02-12T14:04:43+01:00
weight = 40
chapter = true
pre = "<b> </b>"
+++


# Research
  
Notes and first steps to realize a research in Computer Science and Intelligent Systems fields.

## Table of contents

{{% children depth="4" %}}