---
title: "DevOps and Computing Science Journals"
date: 2021-02-17T13:58:35+01:00
draft: true
---

- [ACM Computing Surveys](https://dl.acm.org/journal/csur): 2019 Impact Factor: 7.990 (ranked 4/104 in Computer Science Theory & Methods)


## DevOps

- [Agile ALM DevOps Journal]