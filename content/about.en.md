+++
title = "About"
description = "Compilation of tools and news on Programming, Technological Sovereignty, Privacy and Cybersecurity"
date = "2020-09-26"
aliases = ["about-us", "about-hugo", "contact"]
author = "RobinForGood"
+++

On this web page you will find a compilation of tools and news about Programming, Technological Sovereignty, Privacy and Cybersecurity.

If you would like to collaborate and edit or add more content, write to me at robinforgood@protonmail.com with your proposal. Thank you!