+++
author = "RobinForGood"
title = "Recopilación de herramientas de seguridad"
date = "2021-05-15"
description = ""
featured = true
tags = [
    "GNU/Linux",
    "docker"
]
categories = [
    "Seguridad",
    "herramientas"
]
series = ["Security"]
aliases = ["server"]
thumbnail = "images/seguridad.png"
toc = true
+++

## Hacking
* [Awesome hacking](https://github.com/Hack-with-Github/Awesome-Hacking)
* [Awesome Hacking Resources](https://github.com/vitalysim/Awesome-Hacking-Resources)

## Administración de sistemas
* [Snowflake SSH Client](https://github.com/subhra74/snowflake) es una [navaja suiza para los administradores de sistemas](https://colaboratorio.net/reinaldo-espinosa/sysadmin/2020/snowflake-navaja-suiza-sysadmins/)

## Análisis de red

### Subdominios
[Comparativa de herramientas de enumeración de subdominios:](https://www.hackplayers.com/2020/02/comparativa-tools-subdominios.html)
* [Amass](https://github.com/OWASP/Amass): más lenta pero encuentra más subdominios válidos
* [Findomain](https://github.com/Edu4rdSHL/findomain): más rápida con buen número de subdominios encontrados
* [Subfinder](https://github.com/projectdiscovery/subfinder)
* [Sublist3r](https://github.com/aboul3la/Sublist3r)
* [DNSRecon](https://github.com/darkoperator/dnsrecon)
* [dnssearch](https://github.com/evilsocket/dnssearch)
* [Knock](https://github.com/guelfoweb/knock)
* [SubBrute](https://github.com/TheRook/subbrute)


## Programación segura 

### Revisión de código
#### De pago:
* [Check Marx](https://www.checkmarx.com/) 
* [VeraCode](https://www.veracode.com/) 
* [Fortify](https://www.microfocus.com/en-us/products/static-code-analysis-sast/overview) 
* [AppScan-IBM](https://www.ibm.com/in-en/security/application-security/appscan)
* [Kiuwan](https://www.kiuwan.com/)

## Revisión de dependencias:
* Dependency check
* Kiuwan
* Sonatype
* SourceClear
* BlackDuck
* Snyk

## Secretos hardcodeados (contraseñas en el código)
* Gitleaks