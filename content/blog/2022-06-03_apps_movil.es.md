+++
author = "RobinForGood"
title = "Aplicaciones de móvil"
date = "2022-06-03"
description = ""
featured = true
tags = [
    "f-droid",
    "android",
]
categories = [
    "soberanía tecnológica",
    "privacidad",
    "ciberseguridad",
    "software libre",
    "herramientas",
]
series = ["F-Droid"]
aliases = [""]
thumbnail = "images/f-droid.png"
toc = true
draft = false
+++

En esta entrada se exponen varias aplicaciones android de software libre que pueden descargar desde F-Droid.

## F-Droid

- AntennaPod

- Aurora Store

- Archivos

- Calendario

- Contactos

- F-Droid: repositorios

    - F-Droid Archive

    - F-Droid

    - Guardian Project Archive
    
    - Guardiann Project Official Releases
    
    - Partido Interdimensional Pirata: https://fdroid.partidopirata.com.ar/fdroid/repo?fingerprint=3DF6969EA3A2186D8A5DB00884B3F42F164931E8CFAFD7CC48263CAD1361A1D5
    
    - archive.newpipe.net/fdroid/repo: https://archive.newpipe.net/fdroid/repo?fingerprint=E2402C78F9B97C6C89E97DB914A2751FDA1D02FE2039CC0897A462BDB57E7501

- Fedilab

- Galería

- GitFox

- GitNex

- Grabadora

- Hacker's Keyboard

- IceCatMobile

- Jami

- K-9 Mail

- LibreOffice Viewer

- Librera

- Markor

- MoneyBuster

- Musica

- NewPipe

- Nextcloud

- OpenkeyChain

- OpenTrack

- OSM DashBoard

- OsmAnd~

- Protonmail

- RiseUpVPN

- Standart Notes

- Tutanota

- Untrackme

- VLC