+++
author = "RobinForGood"
title = "Instalación de Debian y ParrotOS con arranque dual en una partición cifrada con LUKS, volúmenes LVM y rEFInd"
date = "2022-01-06"
description = ""
featured = false
tags = [
    "debian",
    "parrot",
    "rEFInd",
    "luks",
    "administración de sistemas"
]
categories = [
    "soberanía tecnológica",
    "seguridad"
]
series = ["GNU/Linux"]
aliases = [""]
thumbnail = "images/debian.png"
toc = true
+++

En esta entrada se instalará y se configurará un arranque dual de múltiples distribuciones de GNU/Linux (Debian y ParrotOS) mediante rEFInd. Si usas GRUB para arrancar múltiples distribuciones y nunca has oído hablar de rEFInd entonces esta publicación es para ti. Hemos seguido las indicación de [este artículo](https://teejeetech.com/2020/09/05/linux-multi-boot-with-refind/).

## Conceptos básicos

Un `boot loader` carga un kernel del sistema operativo en la memoria y lo ejecuta. Un `boot manager` entrega el control a otro programa de arranque. GRUB es tanto un `boot loader` como un `boot manager`. Refind es sólo un `boot manager`.

Otro concepto fundamental es conocer la diferencia entre EFI/UEFI y BIOS.

## GParted

Mediante GParted ejecutado desde un USB live, se eliminan todas las particiones y se crea una nueva tabla de particiones GPT(GUID Partition Table). GPT es un formato utilizado por los sistemas con EFI y es una alternativa moderna al formato MBR(Master Boot Record) que se utiliza en los sistemas con BIOS.

UEFI requiere que cada disco de arranque tenga una partición especial llamada EFI System Partion (ESP). El ESP es una simple partición FAT16 o FAT32 con los flags de partición boot y esp. El ESP almacena archivos ejecutables EFI, a pesar que son de un tamaño inferior a 100MB, algunos sistemas operativos requieren que la partición tenga una capacidad de 500MB. Por lo tanto, se procede a crear una partición de 550MB de tipo primaria, fat32, con la etiqueta ESP y efi como el nombre de la partición. Se aplican los cambios y en la opción de `manage flags` se selecionan boot y esp.

El siguiente paso es crear dos particiones ext4 para los sistemas operativos Debian y ParrotOS. Por ejemplo, se puede asignar 250GB a Debian, 150GB a Parrot y el resto puede ser una partición de datos que sea compartida. A cada partición se recomienda asignar su correspondiente etiqueta.

## Instalación de Debian

Accedemos a la instalación en modo experto con gráfica y avanzamos hasta la detección de discos y particiones. Creamos las siguientes particiones:

- 500MB para una partición `EFI` común. 
- 500MB para la partición `boot` de Debian. 
- 500MB para la partición `boot` de ParrotOS. 
- El resto en una partición donde irán los distintos sistemas operativos.

Primero se crea un volumen cifrado en la partición con la etiqueta `all-Operative-Systems` indicando que no se formatee o se borre la partición. Después se crea un volumen LVM de grupo y los siguientes volúmenes lógicos:

### Volúmenes Debian:
- 8GB para SWAP.
- 250GB para root.
- 100GB para home.

### Volúmenes ParrotOS:
- 8GB para SWAP.
- 250GB para root.
- 100GB para home.

### Volúmenes datos comunes:
- El resto para datos comunes.

Se asigna a cada volumen lógico de Debian el punto de montaje correspondiente y se termina la instalación eligendo el entorno de escritorio al gusto del consumidor.

## Instalación de ParrotOS

Como ParrotOS es una distribución derivada de Debian, la instalación se realiza igual que en el apartado anterior. Se accede al modo experto de instalación gráfica y se avanza hasta el punto de detección de discos. Como la partición de los sistemas operativos está cifrada, es necesario descifrarla y detectar el grupo de volúmenes LVM. Para ello, saldremos de la sección de detección de discos y iremos a la sección de abrir una terminal o shell. Lo primero será desencriptar la partición cifrada con:

Nota: la partición /dev/sdaX debe corresponder a la cifrada y el nombre debe ser el asignado en la etiqueta.

```zsh
cryptsetup luksOpen /dev/sdaX all-Operative-Systems
```

Posteriormente detectaremos el grupo de los volúmenes LVM con:
```zsh
vgchange -a y
```

Nota: es probable que los siguientes pasos no funcionen a la primera y sea necesario completarlos con la siguiente sección. Por lo que se podría obviar el final de esta sección e instalar el grub directamente.

Una vez ejecutados los comandos anteriores, seguir con la instalación hasta la instalación del GRUB. Volvemos a abrir una terminal e identificamos el UUID de la partición cifrada con:

```zsh
blkid /dev/sdaX
```

Lo siguiente es modificar el archivo `/etc/crypttab`:

```zsh
nano /etc/crypttab
```

Y se añade el siguiente contenido, donde el UUID es el obtenido del comando `blkid`:

```
all-Operative-Systems UUID=524c1ad6-fabe-4f32-9bb0-c8db1286b262 none luks
```

Terminamos la instalación y reiniciamos el sistema operativo. Si todo funciona correctamente, ya estaría terminado. Lo más habitual es que al intentar arrancar el sistema operativo no pueda abrir la partición cifrada, ni los volúmenes cifrados. Entonces, abrirá una terminal de initramfs y tendremos que configurar los siguientes pasos.

### No abre la partición cifrada y salta una terminal de initramfs

En caso de obtener una terminal con initramfs, será necesario volver a repetir los pasos de descifrar la partición y abrir el grupo de volúmenes cifrados como se menciona anteriormente. 

Abrimos la partición cifrada con:

```
cryptsetup luksOpen /dev/sdaX all-Operative-Systems
```

Posteriormente detectaremos el grupo de los volúmenes LVM con:

```
vgchange -a y
```

Para arrancar el sistema, simplemente utilizamos el siguiente comando:

```
exit
```

Nos llevará al login y entraremos con las credenciales creadas en la instalación. Una vez iniciado el sistema operativo, abrimos una terminal y
detectamos el UUID de la partición cifrada. La X de sdaX corresponde al número de la partición cifrada, si no se conoce simplemente utilizar el comando `blkid`.

```
blkid /dev/sdaX
```

Editamos el archivo `/etc/crypttab` con nano:

```zsh
sudo nano /etc/crypttab
```

Añadimos lo siguiente:
```
all-Operative-Systems UUID=524c1ad6-fabe-4f32-9bb0-c8db1286b262 none luks
```

Una vez terminado se utiliza el siguiente comando para actualizar initramfs:

```
sudo update-initramfs -u
```

Reiniciamos el sistema operativo con:
```
sudo reboot
```

## Instalación rEFInd

La instalación de rEFInd se realiza con el siguiente comando:

```
sudo apt install refind
```
