+++
author = "RobinForGood"
title = "Configuración de seguridad y privacidad un router"
date = "2022-01-11"
description = ""
featured = false
tags = [
    "router",
    "firewall",
    "redes",
    "administración de sistemas"
]
categories = [
    "soberanía tecnológica",
    "seguridad",
    "privacidad"
]
series = ["Redes"]
aliases = [""]
thumbnail = "images/debian.png"
toc = true
+++

En esta entrada se configurará un router pensando en la seguridad y privacidad.

## Firewall

Activar:

- Denial of Services (DoS) (en caso de que exista esta opción)
- Permitir todo el tráfico de salida.
- Denegar todo el tráfico de entrada.
- Seleccionar IP estática del dispositivo que queramos exponer en la DMZ. Hacer Port forwarding de los puertos que queramos abrir y redireccionar a la IP del dispositivo expuesto. En caso de abrir un puerto para ssh, se recomienda que sea un puerto superior a los 1000 primeros para evitar ataques automatizados.

## Contraseñas

- Cambiar contraseña del WiFi y la contraseña de administración.

## Desativar

- WPS