+++
author = "RobinForGood"
title = "Colección de herramientas"
date = "2020-09-30"
description = "Colección de herramientas de privacidad, hacking, administración de sistemas, análisis de red y programación segura."
featured = true
tags = [
    "herramientas",
    "colección",
    "privacidad",
    "software libre",
    "hacking",
    "análisis de red",
    "programación segura"
]
categories = [
    "colección",
    "herramientas",
]
series = ["Herramientas"]
aliases = ["migrate-from-jekyl"]
thumbnail = "images/tool-software.png"
+++

En esta página están almacenadas herramientas de privacidad, hacking, administración de sistemas, análisis de red y programación segura. La intención es ir actualizando las herramientas y especificar un breve resumen de qué se encuentra en cada enlace. 

## Privacidad o software libre
* [Prism break](https://prism-break.org/es/)
* [Switching.software](https://switching.software/)
* [Desgooglicemos internet](https://degooglisons-internet.org/es/)

### Extensiones para el navegador

* uBlock
* [Privacy redirect](https://addons.mozilla.org/es/firefox/addon/privacy-redirect/)

## Anonimato

* [Guía de anonimato en internet](https://anonymousplanet.org/guide.html)
* Correos temporales: [Tempail](https://tempail.com/en/)
* Recibidores de SMS: [sms24](https://sms24.me/en/), [oksms](https://oksms.org/) ó [fake-sms](https://github.com/Narasimha1997/fake-sms)

## Hacking
* [Awesome hacking](https://github.com/Hack-with-Github/Awesome-Hacking)
* [Awesome Hacking Resources](https://github.com/vitalysim/Awesome-Hacking-Resources)

## Administración de sistemas
* [Snowflake SSH Client](https://github.com/subhra74/snowflake) es una [navaja suiza para los administradores de sistemas](https://colaboratorio.net/reinaldo-espinosa/sysadmin/2020/snowflake-navaja-suiza-sysadmins/)

## Análisis de red

### Subdominios
[Comparativa de herramientas de enumeración de subdominios:](https://www.hackplayers.com/2020/02/comparativa-tools-subdominios.html)
* [Amass](https://github.com/OWASP/Amass): más lenta pero encuentra más subdominios válidos
* [Findomain](https://github.com/Edu4rdSHL/findomain): más rápida con buen número de subdominios encontrados
* [Subfinder](https://github.com/projectdiscovery/subfinder)
* [Sublist3r](https://github.com/aboul3la/Sublist3r)
* [DNSRecon](https://github.com/darkoperator/dnsrecon)
* [dnssearch](https://github.com/evilsocket/dnssearch)
* [Knock](https://github.com/guelfoweb/knock)
* [SubBrute](https://github.com/TheRook/subbrute)


## Programación segura 

### Revisión de código
#### De pago:
* [Check Marx](https://www.checkmarx.com/) 
* [VeraCode](https://www.veracode.com/) 
* [Fortify](https://www.microfocus.com/en-us/products/static-code-analysis-sast/overview) 
* [AppScan-IBM](https://www.ibm.com/in-en/security/application-security/appscan)
* [Kiuwan](https://www.kiuwan.com/)

## Revisión de dependencias:
* Dependency check
* Kiuwan
* Sonatype
* SourceClear
* BlackDuck
* Snyk

## Secretos hardcodeados (contraseñas en el código)
* Gitleaks

## Soberanía tecnológica
* [Colección de herramientas para autoalojar](https://github.com/awesome-selfhosted/awesome-selfhosted)
* [CapRover](https://caprover.com/docs/get-started.html)
* [Cloudron.io](https://www.cloudron.io/)
* [Sandstorm.io](https://sandstorm.io/)
