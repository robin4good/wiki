+++
author = "RobinForGood"
title = "Capitalismo de vigilancia"
date = "2021-01-11"
description = ""
featured = false
tags = [
    "capitalismo de vigilancia",
    "derechos digitales"
]
categories = [
    "privacidad"
]
series = ["Privacidad"]
aliases = [""]
thumbnail = "images/capitalismo-vigilancia.png"
toc = true
weight = 4
+++

## Proyectos interesantes

### Bases de conocimiento

- [Tinfolismo.org](https://tinfoilismo.org/articulos/capitalismo-de-vigilancia/): es una base de conocimiento colaborativa por personas preocupadas por las derivas antihumanas de la tecnología y el mundo digital. Queremos recopilar recursos para la defensa de los derechos digitales y el ejercicio de la libertad, así como guías de elección y autoalojamiento de servicios, guías de mínimos datos y cronologías de sucesos contrarios a la democracia, a los derechos humanos y a la ética cívica.

### Asistentes de voz

- [Sebastian](https://www.xataka.com/accesorios/asistente-que-controla-al-asistente-este-interruptor-voz-genial-invento-para-decidir-cuando-queremos-ser-escuchados): un prototipo español para complementar los asistentes de voz

### Desgooglelización de Internet

- [DE-GOOGLE-IFY INTERNET](https://degooglisons-internet.org/en/)

**¿Cuál es el problema?**

Los gigantes de la web son tan poderosos que ejercen un dominio técnico, económico, cultural y político sobre nuestras sociedades. Estas dominaciones plantean muchos problemas para nuestras libertades:

- El capitalismo de la vigilancia
- Deriva democrática
- Cierre a una visión única de la sociedad
- Centralización de los datos y de la atención

**¿Cuál es la solución?**

Las comunidades de software libre ofrecen alternativas éticas a las plataformas centralizadas por los gigantes de la web: es posible encontrar servicios de confianza que respeten nuestra privacidad. Deconstruir y cambiar nuestros hábitos digitales también significa hacer esfuerzos en el día a día. Aquí tienes algunos enlaces que te ayudarán en tu emancipación:

- Encontrar un alojamiento web de confianza con los CHATONS
- Utilizar un servicio gratuito ofrecido por Framasoft o por los CHATONS
- Encontrar un software gratuito con Framalibre (sólo en francés)
- Utilizar las redes sociales descentralizadas en el Fediverso

- [Proyecto artístico Fango](http://martinnadal.eu/fango/)



## Libros

- [El enemigo conoce el sistema, Marta Peirano](https://traficantes.net/libros/el-enemigo-conoce-el-sistema): Todo lo que no quieres pero necesitas saber sobre el poder, la economía, la sociedad y las telecomunicaciones en la era de la información. La red no es libre, ni abierta ni democrática. Es un conjunto de servidores, conmutadores, satélites, antenas, routers y cables de fibra óptica controlados por un número cada vez más pequeño de empresas. Es un lenguaje y una burocracia de protocolos que hacen que las máquinas hablen, normas de circulación que conducen el tráfico, microdecisiones que definen su eficiencia. Si la consideramos un único proyecto llamado internet, podemos decir que es la infraestructura más grande jamás construida, y el sistema que define todos los aspectos de nuestra sociedad. Y sin embargo es secreta. Su tecnología está oculta, enterrada, sumergida o camuflada; sus algoritmos son opacos; sus microdecisiones son irrastreables. Los centros de datos que almacenan y procesan la información están ocultos y protegidos por armas, criptografía, propiedad intelectual y alambre de espino. La infraestructura crítica de nuestro tiempo está fuera de nuestra vista. No podemos comprender la lógica, la intención y el objetivo de lo que no vemos. Todas las conversaciones que tenemos sobre esa infraestructura son en realidad conversaciones sobre su interfaz, un conjunto de metáforas que se interpone entre nosotros y el sistema. Un lenguaje diseñado, no para facilitar nuestra comprensión de esa infraestructura, sino para ofuscarla. El enemigo conoce el sistema pero nosotros no. Este libro te ayudará a conocerlo, y a comprender por qué la herramienta más democratizadora de la historia se ha convertido en una máquina de vigilancia y manipulación de masas al servicio de regímenes autoritarios. Solo así podremos convertirla en lo que más falta nos hace: una herramienta para gestionar la crisis que se avecina de la manera más humana posible. No tenemos un segundo que perder.

- [La era del capitalismo de la vigilancia, Shoshana Zuboff](https://traficantes.net/libros/la-era-del-capitalismo-de-la-vigilancia): En esta obra magistral por la originalidad de las ideas y las investigaciones en ella expuestas, Shoshana Zuboff nos revela el alarmante fenómeno que ella misma ha denominado «capitalismo de la vigilancia». Está en juego algo de la máxima importancia: toda una arquitectura global de modificación de la conducta amenaza con transfigurar la naturaleza humana misma en el siglo XXI de igual modo a como el capitalismo industrial desfiguró el mundo natural en el siglo XX.

    Gracias al análisis de Zuboff, cobran gráficamente vida para nosotros las consecuencias del avance del capitalismo de la vigilancia desde su foco de origen en Silicon Valley hacia todos los sectores de la economía. Hoy se acumula un enorme volumen de riqueza y poder en unos llamados «mercados de futuros conductuales» en los que se compran y se venden predicciones sobre nuestro comportamiento, y hasta la producción de bienes y servicios se supedita a un nuevo «medio de modificación de la conducta».

    La amenaza que se cierne sobre nosotros no es ya la de un Estado «Gran Hermano» totalitario, sino la de una arquitectura digital omnipresente: un «Gran Otro» que opera en función de los intereses del capital de la vigilancia. El exhaustivo y turbador análisis de Zuboff pone al descubierto las amenazas a las que se enfrenta la sociedad del siglo XXI: una «colmena» controlada y totalmente interconectada que nos seduce con la promesa de lograr certezas absolutas a cambio del máximo lucro posible para sus promotores, y todo a costa de la democracia, la libertad y nuestro futuro como seres humanos.

    Sin apenas resistencia en la legislación o en la sociedad, el capitalismo de la vigilancia va camino de dominar el orden social y determinar el futuro digital... si no se lo impedimos antes.

- [Libros como "La Jaula de Confort", "Tensión en la Red", “Cultura Libre: Libertad y control en la era digital” de Esteban Magnani](https://www.estebanmagnani.com.ar/libros/), todos con licencias libres.

## Infografía

- [Colectivo Disonancia](https://colectivodisonancia.net/infografias/)

## Fanzines

- [Colectivo Disonancia](https://colectivodisonancia.net/zines/)