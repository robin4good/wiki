+++
author = "RobinForGood"
title = "Teoría de Señales"
date = "2022-08-22T16:18:33+02:00"
description = "Notas sobre que todo "
featured = true
tags = [
    "Ética I"
]
categories = [
    "Filosofía"
]
series = ["Filosofía"]
aliases = ["filosofía"]
thumbnail = "images/señal.png"
toc = true
weight = 10
draft=false
+++


# Teoría de señales



> En biología evolutiva, la teoría de señales o de señalización es un cuerpo de trabajo teórico que examina la comunicación entre individuos. La cuestión central se presenta cuando se debe esperar que los organismos con intereses en conflicto se comuniquen con honestidad (considerando que no hay una intención consciente) en lugar de deshonestamente. Los modelos matemáticos en los que los organismos señalan su condición a otros individuos como parte de una estrategia evolutivamente estable son importantes para la investigación en este campo.

> Las señales se dan en contextos tales como la selección de pareja por las hembras, lo cual somete a las señales de los machos a presión selectiva. Las señales evolucionan porque modifican el comportamiento del receptor para beneficiar al comunicador. Las señales pueden ser honestas, transmitiendo información que aumenta de manera la aptitud del receptor, o deshonestas. Un individuo puede engañar a otro al hacer una señal deshonesta, lo que podría beneficiar brevemente al comunicador, con el riesgo de socavar el sistema de señalización para toda la población.

Ejemplos:

Un springbok o gacela saltarina (Antidorcas marsupialis) puede hacer una señal honesta a depredadores como los guepardos para indicar que es un individuo ágil y rápido, y por tanto no valdría la pena perseguirlo.

[Teoría de señales, Wikipedia](https://es.wikipedia.org/wiki/Teor%C3%ADa_de_se%C3%B1ales)