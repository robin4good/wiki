---
title: "Learn theme"
date: 2021-02-13T12:45:35+01:00
draft: false
weight: 10
---

"Hugo-theme-learn  is a theme for Hugo, a fast and modern static website engine written in Go. Where Hugo is often used for blogs, this multilingual-ready theme is fully designed for documentation"<cite> [^1]</cite>. The [GitHub repo of Learn](https://github.com/matcornic/hugo-theme-learn) has MIT license.

1. Create a new chapter with:
```
hugo new --kind chapter hugo/_index.md
```

2. Create a new entry.
```
hugo new hugo/quick_start.md
```


[^1]: [Hugo-theme-learn, Documentation](https://learn.netlify.app/en/)
