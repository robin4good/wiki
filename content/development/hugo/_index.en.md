+++
title = "Hugo"
date = 2021-02-13T12:17:37+01:00
weight = 10
chapter = true
pre = "<b> </b>"
+++

# Hugo

First steps to create and deploy a Hugo project in GitLab pages.

## Table of contents

{{% children depth="4" %}}