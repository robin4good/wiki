+++
title = "DevOps"
date = 2021-03-03T10:36:18+01:00
weight = 25
chapter = true
pre = "<b> </b>"
+++

# DevOps

Notes about DevOps principles, practices, standardization efforts, tools, ...

## Table of contents

{{% children depth="4" %}}