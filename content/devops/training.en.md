---
title: "Training resources"
date: 2021-03-05T10:46:34+01:00
draft: false
weight: 15
---

## Cloud

- [Cloud Training Resources](https://my.usgs.gov/confluence/display/cdi/Cloud+Training+Resources)

## Kubernetes

- [Kube academy](https://kube.academy/)

- [Katakoda](https://www.katacoda.com/?q=kubernetes&hPP=12&idx=scenarios&p=0&is_v=1)

- [A visual guide on troubleshooting Kubernetes deployments](https://learnk8s.io/troubleshooting-deployments)

- [Técnicas avanzadas de Scheduling](https://www.youtube.com/watch?v=OiOfpkiOK0I)