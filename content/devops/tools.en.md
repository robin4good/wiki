---
title: "Tools"
date: 2021-03-03T10:46:34+01:00
draft: true
---

### Toolchain

- [Considerations for Your DevOps Toolchain](https://dzone.com/articles/considerations-for-your-devops-toolchain)

### CD

- [Tekton](https://tekton.dev/). 
  - [GitHub](https://github.com/tektoncd)
  - [Playground](https://katacoda.com/tektoncd/scenarios/playground)
  - Licence: Apache 2.0

